import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router';
import { BrowserRouter as Router } from 'react-router-dom';
import { ConnectPanel } from './ConnectPanel';
import { DocsPageContainer } from './DocsPageContainer';

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <Switch>
            <Redirect  exact={true} path="/" to="/docs/home" />
            <Route path="/panel" component={ConnectPanel} />
            <Route path="/docs/:page" component={DocsPageContainer} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
