import * as React from 'react';
import { PageContextProps } from '../page-context';
import { RouteComponentProps, Switch, Route } from 'react-router';
import { CreateAddCommentContainer } from './CreateAddCommentContainer';
import { ViewAddCommentContainer } from './ViewAddCommentContainer';
import { EditAddCommentContainer } from './EditAddCommentContainer';

export type Props = RouteComponentProps<void> & PageContextProps;

export class AddCommentContainer extends React.PureComponent<Props> {
   render() {
      const { pageContext } = this.props;
      const matchUrl = this.props.match.url;
      console.log(`AddCommentContainer match url: ${matchUrl}`);
      return (
         <Switch>
            <Route
               path={`${matchUrl}/create`}
               render={(props) => <CreateAddCommentContainer {...props} pageContext={pageContext} />}
            />
            <Route
               path={`${matchUrl}/edit`}
               component={EditAddCommentContainer}
            />
            <Route
               path={`${matchUrl}/view`}
               component={ViewAddCommentContainer}
            />
         </Switch>
      );
   }
}