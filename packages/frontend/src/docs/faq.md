# Frequently Asked Questions

These are the most common questions that we get about the workflow-reactions service?

## Can I set a reaction to happen a set time after and event has ocurred?

Unfortunately no, not yet. This may be a feature that we build in the future.

## What data do you store in your service?

We store the initial installation details of the App. Other than that, all of our storage
of data is done inside Jira. We don't store any personal data or user generated content.

## What external services do you rely upon?

This service is hosted on AWS. We use CloudFront and Api Gateway / Lambdas to handle most
of the event based workload.