import * as React from 'react';
import { storiesOf, Story } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { CreateAddComment } from '../add-comment/CreateAddComment';
import { CommentTemplateContext } from '../add-comment/comment-context';

type partialStoriesOf = (name: string) => Story;

export function compileStories() {
  const localStoryCreator: partialStoriesOf = (name: string) => storiesOf(name, module);
  stories(localStoryCreator);
}

export default function stories(storyCreator: partialStoriesOf) {
   storyCreator('Add Comment/Create')
      .add('Default', () => {
         const context: CommentTemplateContext = {
            issue: {
               key: 'DEMO-1234',
               fields: {
                  summary: 'This is an issue'
               }
            }
         };
         return <CreateAddComment context={context} />;
      })
}