# About Workflow Reactions for Jira

Workflow reactions for Jira was originally written to help manage Atlassian ShipIt issues. We
wanted to make sure that, once ShipIt was complete, that teams could understand how they should
continue to convey the progress of their projects. The initial release was written by a single
developer in less than one week. At the time, it just had one workflow reaction: "Add comment".

## Developers

 - Robert Massaioli (rmassaioli@atlassian.com)