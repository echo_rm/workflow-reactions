import { APIGatewayProxyHandler, APIGatewayProxyResult } from 'aws-lambda';
import { DynamoDB } from 'aws-sdk';
import { createDocumentCilent } from './dynamodb-client';
import * as jwt from 'atlassian-jwt';
import { extractJwtToken } from './jwt';
import { LifecyclePayload } from './types';
import { error } from './gateway-util';

const dynamoDb = createDocumentCilent();

// TODO Swap this out to validate the expected payload with JSON Schemas
function validateLifecycle(input: any): input is LifecyclePayload {
  return input !== undefined &&
    typeof input.key === 'string' &&
    typeof input.clientKey === 'string' &&
    (input.sharedSecret === undefined || typeof input.sharedSecret === 'string') &&
    typeof input.serverVersion === 'string' &&
    typeof input.pluginsVersion === 'string' &&
    typeof input.baseUrl === 'string' &&
    typeof input.productType === 'string' &&
    typeof input.description === 'string' &&
    ['installed', 'uninstalled', 'enabled', 'disabled'].includes(input.eventType);
}

export const lifecycle: APIGatewayProxyHandler = async (event, _context) => {
  const installTable = process.env.INSTALL_TABLE;
  if (installTable === undefined) {
    return error(500, 'The installation table was configured incorrectly. Please contact the app developer.');
  }

  // Parse the payload from the body.
  if (event.body === null) {
    return error(400, 'No body on the incoming lifecycle event.');
  }
  const input = JSON.parse(event.body);
  // Validate the incoming lifecycle payload is a lifecycle payload
  if (!validateLifecycle(input)) {
    return error(400, 'Invalid lifecycle payload sent to the function.');
  }
  // Ignore all non-install events
  if (input.eventType !== 'installed') {
    return {
      statusCode: 204,
      body: ''
    };
  }
  // With the clientKey from the lifecycle payload, lookup a matching item in storage
  const getTenantParams: DynamoDB.DocumentClient.GetItemInput = {
    TableName: installTable,
    Key: {
      clientKey: input.clientKey
    }
  };

  const lifecycleLookup = await dynamoDb.get(getTenantParams).promise();
  const lifecycleLookupResponse = lifecycleLookup.$response;

  if (lifecycleLookupResponse.error) {
    return error(500, 'Failed to connect to our persistence layer, failed to handle your request');
  }
  // If you can not find one, then save the new install details
  if (lifecycleLookup.Item === undefined) {
    // Save the new install details
    const savePayload: DynamoDB.DocumentClient.PutItemInput = {
      TableName: installTable,
      Item: input
    };

    const saveAction = await dynamoDb.put(savePayload).promise();
    if (saveAction.$response.error) {
      return error(500, 'Failed to save your tenant data to the storage layer.');
    }
    return { statusCode: 204, body: '' };
  } else {
    const existingLifecycle: LifecyclePayload = lifecycleLookup.Item as LifecyclePayload;
    if (!validateLifecycle(lifecycleLookup.Item)) {
      return error(500, 'The loaded lifecycle event was not in the format that we expected.');
    }
    // Get the JWT token that should be on the request
    const jwtToken = extractJwtToken(event);
    if (jwtToken === undefined) {
      return error(400, 'We already know of this tenant and expect a JWT token to be provided.');
    }
    //   validate the JWT token on the request against the shared secret
    try {
      jwt.decode(jwtToken, existingLifecycle.sharedSecret);
    } catch (e) {
      return error(403, 'The token that you provided was invalid.');
    }

    //   if it matches, perform an update of the storage
    const savePayload: DynamoDB.DocumentClient.PutItemInput = {
      TableName: installTable,
      Item: input
    };

    const saveAction = await dynamoDb.put(savePayload).promise();
    if (saveAction.$response.error) {
      return error(500, 'Failed to save your tenant data to the storage layer.');
    }
    return { statusCode: 204, body: '' };
  }
};