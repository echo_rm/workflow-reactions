export type CommentTemplateContext = {
  issue: IssueContext;
};

export type IssueContext = {
  key: string;
  fields: IssueFields;
};

export type IssueFields = {
  summary: string
}

export const ExampleContext: CommentTemplateContext = {
  issue: {
    key: 'DEMO-1234',
    fields: {
      summary: 'A demo issue summary!'
    }
  }
}