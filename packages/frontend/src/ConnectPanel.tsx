import * as React from 'react';
import { Route, Switch, RouteComponentProps } from 'react-router-dom';
import { loadPageContext, PageContext } from './page-context';
import Spinner from '@atlaskit/spinner';
import { AddCommentContainer } from './add-comment/AddCommentContainer';

type ConnectPanelState = {
  pageContext: PageContext | undefined;
  apLoaded: boolean;
};

export class ConnectPanel extends React.PureComponent<RouteComponentProps<void>, ConnectPanelState> {
  componentWillMount() {
    const pc = loadPageContext();

    this.setState({
      pageContext: pc,
      apLoaded: false
    });

    const script = document.createElement('script');
    script.src = `https://connect-cdn.atl-paas.net/all.js`;

    script.onload = () => {
      this.setState(s => {
        return {
          ...s,
          apLoaded: true
        };
      });
    };

    document.getElementsByTagName('head')[0].appendChild(script);
  }

  componentDidUpdate() {
    if (this.state.apLoaded) {
      AP.resize();
    }
  }

  render() {
    const pc = this.state.pageContext;
    if (pc === undefined) {
      // TODO clean this up and load the page context correctly
      return <div>Error state: no page context!</div>;
    } else if (!this.state.apLoaded) {
      return <Spinner delay={100} size="large" />;
    }

    const matchUrl = this.props.match.url;
    console.log(`Match url connect panel: ${matchUrl}`);

    return (
      <div className="ac-content" style={{ boxSizing: 'border-box', overflow: 'hidden'}}>
        <Switch>
          <Route
            path={`${matchUrl}/add-comment`}
            render={(props) => <AddCommentContainer {...props} pageContext={pc} />}
          />
        </Switch>
      </div>
    );
  }
}