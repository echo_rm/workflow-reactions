import * as React from 'react';
import { PageContextProps } from '../page-context';
import { RouteComponentProps } from 'react-router';
import { ExampleContext } from './comment-context';
import { CommentPreview } from './CommentPreview';
import styled from 'styled-components';

export type Props = RouteComponentProps<void> & PageContextProps;

type State = {
   commentTemplate: string;
};

export class ViewAddCommentContainer extends React.PureComponent<Props, State> {
   private static TopLine = styled.p`
      margin-top: 0;
      padding-top: 0;
   `;

   state = {
      commentTemplate: 'Loading...'
   };

   componentDidMount() {
      AP.jira.getWorkflowConfiguration(config => {
         const parsedConfig = JSON.parse(config);
         const commentTemplate = parsedConfig.commentTemplate;
         if (typeof commentTemplate === 'string') {
            this.setState({
               commentTemplate
            });
         }
      });
   }

   render() {
      const { TopLine } = ViewAddCommentContainer;
      return (
         <>
            <TopLine>Add this comment:</TopLine>
            <CommentPreview commentTemplate={this.state.commentTemplate} context={ExampleContext} />
         </>
      );
   }
}