## Workflow Reactions for Jira

This Atlassian App was developed extend Jira Workflows with actions that are useful to administrators of projects
that want to perform more automation.

Currently it adds the following workflow features.

### Post Function

 - **Add Comment**
   Add a comment to an Issue after a workflow transition on that issue.

### Potential Future Ideas

 - **Add comment to linked issues**  
   Once an issue transitions, make a comment on all of the linked issues.
 - **Scheduled update after transition**  
   Make an update to an issue a set amount of time after it transitioned.