import * as React from 'react';
import { PageContextProps } from '../page-context';
import { RouteComponentProps } from 'react-router';
import { CreateAddComment } from './CreateAddComment';
import { CommentTemplateContext } from './comment-context';

export type Props = RouteComponentProps<void> & PageContextProps;

export class CreateAddCommentContainer extends React.PureComponent<Props> {
   render() {
      // TODO the template format should be mustache but with '<<' and '>>' as the tags.
      console.log('Render the CreateAddComment');
      const context: CommentTemplateContext = {
         issue: {
            key: 'DEMO-1234',
            fields: {
               summary: 'A demo issue summary'
            }
         }
      };
      return <CreateAddComment context={context} />;
   }
}