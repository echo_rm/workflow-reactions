import * as React from 'react';
import { PageContextProps } from '../page-context';
import { RouteComponentProps } from 'react-router';
import { ExampleContext } from './comment-context';
import styled from 'styled-components';
import { CreateAddComment } from './CreateAddComment';
import Spinner from '@atlaskit/spinner';

export type Props = RouteComponentProps<void> & PageContextProps;

type State = {
   commentTemplate?: string;
};

export class EditAddCommentContainer extends React.PureComponent<Props, State> {
   private static TopLine = styled.p`
      margin-top: 0;
      padding-top: 0;
   `;

   state = {
      commentTemplate: undefined
   };

   componentDidMount() {
      AP.jira.getWorkflowConfiguration(config => {
         const parsedConfig = JSON.parse(config);
         const commentTemplate = parsedConfig.commentTemplate;
         if (typeof commentTemplate === 'string') {
            this.setState({
               commentTemplate
            });
         }
      });
   }

   render() {
      const { commentTemplate } = this.state;
      if (commentTemplate === undefined) {
         return <Spinner />;
      }
      return (
         <CreateAddComment initialCommentTemplate={commentTemplate} context={ExampleContext} />
      );
   }
}