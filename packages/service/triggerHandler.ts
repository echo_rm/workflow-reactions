import { APIGatewayProxyHandler } from "aws-lambda";
import { loadTenant, isTenantLoadError } from "./jwt";
import { createDocumentCilent } from "./dynamodb-client";
import { error } from "./gateway-util";
import * as jwt from 'atlassian-jwt';
import * as moment from 'moment';
import * as WebRequest from 'web-request';
import * as Mustache from 'mustache';

const dynamoDb = createDocumentCilent();

type TriggerPayload = {
  issue: {
    key: string;
    fields: {
      summary: string;
    };
  };
  configuration: { value: string };
  transition: {
    "to_status": string;
    transitionId: number;
    workflowName: string;
    "from_status": string;
    workflowId: number;
    transitionName: string;
  };
}

export const addCommentTrigger: APIGatewayProxyHandler = async (event, context) => {
  const key = process.env.APP_KEY;
  if (key === undefined) {
    return error(500, 'The app key configuration is missing. Please contact the app developer.');
  }

  // Validate the incoming request
  const tenant = await loadTenant(event, dynamoDb);
  if (isTenantLoadError(tenant)) {
    return error(400, tenant.error);
  }

  // Parse the incoming request
  if (event.body === null) {
    return error(400, 'Expected there to be a body on the incoming request.');
  }

  const parsedBody = JSON.parse(event.body) as TriggerPayload;

  // Create a comment response payload
  const commentTemplate = JSON.parse(parsedBody.configuration.value).commentTemplate;

  let renderedPreview: string | undefined = undefined;
  try {
    renderedPreview = Mustache.render(
      commentTemplate,
      parsedBody,
      {},
      ['<<', '>>']
    );
  } catch (e) {
    return error(400, `Could not render the comment template. ${e}`);
  }

  var payload = {
    "body": renderedPreview
  };

  const urlPath = `/rest/api/2/issue/${parsedBody.issue.key}/comment`;

  // Sign the outbound comment
  const jwtRequest = jwt.fromMethodAndUrl('POST', urlPath);

  const now = moment();

  const tokenData = {
    "iss": key,
    "iat": now.unix(),                      // the time the token is generated
    "exp": now.add(3, 'minutes').unix(),    // token expiry time (recommend 3 minutes after issuing)
    "qsh": jwt.createQueryStringHash(jwtRequest)   // [Query String Hash](https://developer.atlassian.com/cloud/jira/platform/understanding-jwt/#a-name-qsh-a-creating-a-query-string-hash)
  };

  const encodedToken = jwt.encode(tokenData, tenant.sharedSecret);

  // Send the comment back to the instance
  //  /rest/api/3/issue/{issueIdOrKey}/comment
  const response = await WebRequest.post(`${tenant.baseUrl}${urlPath}`, {
    headers: {
      'Authorization': `JWT ${encodedToken}`,
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(payload)
  });

  if (200 >= response.statusCode || response.statusCode >= 300) {
    return error(500, `Failed to update Jira. ${response.content}`);
  }

  // Return successful

  return { statusCode: 204, body: '' };
};