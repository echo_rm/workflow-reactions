import { APIGatewayProxyResult } from "aws-lambda";

export function error(statusCode: number, errorMessage: string): APIGatewayProxyResult {
  // You can go and see these errors in cloudwatch.
  console.error(`HTTP ${statusCode}: ${errorMessage}`);
  return {
    statusCode,
    body: JSON.stringify({ error: errorMessage }, null, 2)
  };
}