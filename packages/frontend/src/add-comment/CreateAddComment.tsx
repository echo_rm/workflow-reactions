import * as React from 'react';
import FieldTextArea from '@atlaskit/field-text-area';
import { throttle } from 'throttle-debounce';
import { CommentTemplateContext } from './comment-context';
import { CommentPreview } from './CommentPreview';

export type Props = {
  context: CommentTemplateContext;
  initialCommentTemplate?: string;
};

type SaveState = {
  commentTemplate: string;
}

type State = SaveState & {
  templateHasError: boolean;
  lastSuccessfulPreview: string;
};

export class CreateAddComment extends React.PureComponent<Props, State> {
  private throttledTriggerSave = throttle<() => void>(250, () => this.triggerSave());

  state = {
    commentTemplate: '',
    lastSuccessfulPreview: '',
    templateHasError: false
  };

  componentDidMount() {
    if (this.isApDefined()) {
      AP.jira.WorkflowConfiguration.onSaveValidation(() => {
        return !this.state.templateHasError;
      });

      AP.jira.WorkflowConfiguration.onSave(() => {
        const saveState: SaveState = {
          commentTemplate: this.state.commentTemplate
        };
        return JSON.stringify(saveState);
      });
    }

    if (this.props.initialCommentTemplate) {
      this.setState({
        commentTemplate: this.props.initialCommentTemplate
      });
    }
  }

  render() {
    const { initialCommentTemplate, context } = this.props;
    return (
      <>
        <FieldTextArea
          autoFocus
          value={initialCommentTemplate || ''}
          label="Type your comment here. (Use <<issue.key>> to inject the issue key)"
          onChange={e => this.onChange(e)}
          minimumRows={8}
          shouldFitContainer={true}
        />

        <h3>Example</h3>
        <CommentPreview commentTemplate={this.state.commentTemplate} context={context}/>
      </>
    );
  }

  private onChange(event: React.SyntheticEvent<HTMLTextAreaElement>) {
    const commentTemplate = event.currentTarget.value;

    console.log(`New comment template: ${commentTemplate}`);

    this.setState({
      commentTemplate
    });

    this.throttledTriggerSave();
  }

  private triggerSave() {
    if (this.isApDefined()) {
      AP.jira.WorkflowConfiguration.trigger();
    }
  }

  private isApDefined(): boolean {
    return (window as any).AP !== undefined;
  }
}