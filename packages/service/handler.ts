import { APIGatewayProxyHandler } from 'aws-lambda';

export const hello: APIGatewayProxyHandler = async (event, context) => {
  return {
    statusCode: 200,
    body: JSON.stringify({
      message: 'Go Serverless Webpack (Typescript) v1.0! Your function executed successfully!',
      input: event,
    }),
  };
};

function stageWrap(s: string, stage: string) {
  if (stage === 'production') {
    return s;
  }
  return `(${stage}) ${s}`;
}

export const descriptor: APIGatewayProxyHandler = async (event, context) => {
  const stage = event.requestContext.stage || 'local';
  const sw = (s: string) => stageWrap(s, stage);
  const key = process.env.APP_KEY;
  let name = process.env.APP_NAME || 'Workflow reactions for Jira';
  if (name.endsWith('(production)')) {
    name = name.replace(/ \(production\)$/, '');
  }
  const baseUrl = `https://${process.env.BASE_DOMAIN}`;
  const links = {
    self: `${baseUrl}/api/descriptor`
  };
  return {
    statusCode: 200,
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      key,
      name,
      description: sw('React to Jira Workflow events.'),
      baseUrl,
      vendor: {
        name: 'Robert Masssaioli',
        url: 'https://rmdir.app'
      },
      enableLicensing: false,
      scopes: [
        'READ', 'WRITE'
      ],
      authentication: {
        type: 'jwt'
      },
      lifecycle: {
        installed: '/api/app/lifecycle',
        disabled: '/api/app/lifecycle',
        uninstalled: '/api/app/lifecycle',
        enabled: '/api/app/lifecycle'
      },
      apiMigrations: {
        gdpr: true
      },
      modules: {
        jiraWorkflowPostFunctions: [{
          key: 'add-comment-post-function',
          name: {
            value: sw('Add comment')
          },
          description: {
            value: 'React to a workflow transition by leaving a comment on the issue when it reaches a certain status.'
          },
          view: {
            url: '/panel/add-comment/view'
          },
          edit: {
            url: '/panel/add-comment/edit'
          },
          create: {
            url: '/panel/add-comment/create'
          },
          triggered: {
            url: '/api/add-comment/triggered'
          }
        }]
      },
      links
    })
  };
};