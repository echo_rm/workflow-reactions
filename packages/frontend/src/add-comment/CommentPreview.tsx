import * as React from 'react';
import styled from 'styled-components';
import Mustache from 'mustache';
import { CommentTemplateContext } from './comment-context';

interface PreviewProps {
  isInError: boolean;
};

const Preview = styled.div<PreviewProps>`
  border-style: dashed;
  border-width: 1px;
  border-color: ${props => props.isInError ? 'red' : '#ccc'};
  padding: 0.5em;
  color: #091E42;
  margin: 0.5em;
`;

const WrappingPre = styled.pre`
  overflow-x: auto;
  white-space: pre-wrap;
  white-space: -moz-pre-wrap;
  white-space: -pre-wrap;
  white-space: -o-pre-wrap;
  word-wrap: break-word;
`;

export type CommentPreviewProps = {
  commentTemplate: string;
  context: CommentTemplateContext;
};

export const CommentPreview: React.SFC<CommentPreviewProps> = props => {
  const { commentTemplate, context } = props;

  let renderedPreview: string | undefined = undefined;
  try {
    renderedPreview = Mustache.render(
      commentTemplate,
      context,
      {},
      ['<<', '>>']
    );
  } catch (e) {
    // what should we do on error? Nothing.
  }

  return (
    <Preview isInError={renderedPreview === undefined}>
      <WrappingPre><code>{renderedPreview === undefined ? commentTemplate : renderedPreview}</code></WrappingPre>
    </Preview>
  );
};