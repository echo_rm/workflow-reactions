I first created a Serverless app

Then I wanted to figure out how to serve my frontend code. For that, I put the API 
Gateway and an S3 bucket behind a CloudFront entry point. To do this, I had to learn 
how CloudFormation works and how you inject cloudformation extra templates under the 
Resources section of your serverless.yaml functions. I also learned about the custom 
resource names used by serverless that can be used as API.

In order to deploy the frontend code, post-deploy, just run this in the `packages/service` directory:

    yarn sls client deploy

Now, the next step is to try and return an app descriptor from serverless. To do that, 
we are going to have to be able to work out what the baseUrl should be. It looks like 
we can set environment variables for functions, maybe that will help: 
https://serverless.com/framework/docs/providers/aws/guide/functions#environment-variables
That ended up working in the end and we can use it to setup a route and a certificate.

For the domain name, I had to custom create A and AAAA records in Route 53 and then update
CloudFront to use that domain name AND the *.rmdir.app wildcard certificate. That made
my service available with HTTP. This is pretty great.

Now I am in the process of setting up a locally running development experience that operates
the same way as cloudfront does. Having a local development loop would be awesome. I am
thinking that I will setup my own proxy server using npm http-proxy and making it proxy both
my frontend and my backend.

I successfully did this by writing the `local-proxy.ts` script and by setting up hardcoded
ports for everything. The ports are as follows:

 - 9220: The proxy lives here.
 - 9221: The frontend static content provider lives here. (Run `yarn start` in `packages/frontend`)
 - 9222: `serverless-offline` will run here.
 - 9223: `serverless-dynamodb-local` will run here.

In the end, you only need to run two commands to make this happen:

 - In `packages/service`: `yarn start`
 - In `packages/frontend`: `yarn start`

Downloaded icon here: https://www.flaticon.com/free-icon/bounce_637757#term=bounce&page=1&position=17

Credit required is: 

    <div>Icons made by <a href="https://www.flaticon.com/authors/those-icons" title="Those Icons">Those Icons</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>

Open Questions:
 - How do we setup a custom domain name?
 - How do we pass the custom domain name to a serverless function?
 - How do we ensure 
 