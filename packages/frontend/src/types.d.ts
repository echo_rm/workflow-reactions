declare module '@atlaskit/avatar' {
  type strFunc = () => string;
  interface BaseProps {
    appearance: 'circle' | 'square';
    size: 'xsmall' | 'small' | 'medium' | 'large' | 'xlarge' | 'xxlarge';
    enableTooltip?: boolean;
    borderColor?: string | strFunc;
    href?: string;
    isActive?: boolean;
    isDisabled?: boolean;
    isFocus?: boolean;
    isHover?: boolean;
    isSelected?: boolean;
    name?: string;
    src?: string;
    status?: 'approved' | 'declined' | 'locked';
    target?: '_blank' | '_self' | '_top' | '_parent';
    onClick?: () => void;
  }

  export default class Avatar extends React.Component<BaseProps> { }
}

declare module '@atlaskit/page-header' {
  const ex: React.ComponentType<any>;
  export default ex;
}

declare module '@atlaskit/analytics-next' {
  // These types were mostly copied from the media-card Atlaskit package.
  // See https://bitbucket.org/atlassian/atlaskit-mk-2/src/master/packages/media/media-card/src/analytics-next.ts

  type ObjectType = { [key: string]: any };

  type AnalyticsEventPayload = {
    action: string;
    [key: string]: any;
  };

  type AnalyticsEventUpdater =
  | AnalyticsEventPayload
  | ((payload: AnalyticsEventPayload) => AnalyticsEventPayload);

  export type AnalyticsEventProps = {
    action: string;
    payload: AnalyticsEventPayload;
  };

  export interface AnalyticsEventInterface {
    payload: AnalyticsEventPayload;

    clone: () => AnalyticsEventInterface;

    update(updater: AnalyticsEventUpdater): AnalyticsEventInterface;
  }

  type ChannelIdentifier = string;

  interface UIAnalyticsEventHandlerSignature {
    (event: UIAnalyticsEventInterface, channel?: ChannelIdentifier): void;
  }

  export type UIAnalyticsEventProps = AnalyticsEventProps & {
    context?: Array<ObjectType>;
    handlers?: Array<UIAnalyticsEventHandlerSignature>;
  };
  export interface UIAnalyticsEventInterface {
    context: Array<ContextItem>;
    handlers?: Array<UIAnalyticsEventHandlerSignature>;
    hasFired: boolean;
    payload: AnalyticsEventPayload;

    clone: () => UIAnalyticsEventInterface | null;

    fire(channel?: ChannelIdentifier): void;

    update: AnalyticsEventUpdateFunction;
  }

  export interface ContextItem {
    component: string;
    containerId?: string;
    containerType?: string;
    source?: string;
    attributes?: object;
  }

  export type AnalyticsAttributesPayload = {
    [key: string]: string | boolean | number | null | undefined;
  };

  type AnalyticsEventUpdateFunction = (updater: AnalyticsEventUpdater) => UIAnalyticsEventInterface

  export interface AnalyticsListenerProps {
    children?: React.ReactNode;
    channel?: string;
    onEvent: (event: UIAnalyticsEventInterface, channel?: string) => void;
  }

  export class AnalyticsListener extends React.Component<
    AnalyticsListenerProps
  > {}

  export interface AnalyticsContextProps {
    children: React.ReactNode;
    data: ObjectType;
  }

  export class AnalyticsContext extends React.Component<
    AnalyticsContextProps
  > {}

  export type WithAnalyticsContextProps = {
    analyticsContext?: ObjectType;
  };

  export type WithAnalyticsContextFunction = <TOwnProps>(
    component: React.ComponentClass<TOwnProps>,
  ) => React.ComponentClass<TOwnProps & WithAnalyticsContextProps>;

  export function withAnalyticsContext(
    defaultData?: any,
  ): WithAnalyticsContextFunction;

  export type CreateUIAnalyticsEventSignature = (
    payload?: AnalyticsEventPayload,
  ) => UIAnalyticsEventInterface;

  export interface EventMap<TOwnProps> {
    [k: string]:
      | ObjectType
      | ((
          create: CreateUIAnalyticsEventSignature,
          props: TOwnProps,
        ) => UIAnalyticsEventInterface | void);
  }

  export interface WithAnalyticsEventProps {
    createAnalyticsEvent: CreateUIAnalyticsEventSignature;
  }

  export type WithAnalyticsEventFunction = <TOwnProps>(
    component: React.ComponentClass<WithAnalyticsEventProps & TOwnProps> | React.SFC<WithAnalyticsEventProps & TOwnProps>,
  ) => React.ComponentClass<TOwnProps>;

  export function withAnalyticsEvents<TOwnProps>(
    createEventMap?: EventMap<TOwnProps>,
  ): WithAnalyticsEventFunction;

  export type CreateAndFireEventFunction = (
    payload: AnalyticsEventPayload,
  ) => (
    createAnalyticsEvent: CreateUIAnalyticsEventSignature,
  ) => UIAnalyticsEventInterface;

  export function createAndFireEvent(
    channel?: string,
  ): CreateAndFireEventFunction;
}

declare module '@atlaskit/banner' {
  type BannerProps = {
    appearance?: 'warning' | 'error';
    icon?: React.ReactNode;
    isOpen?: boolean;
  }

  export default class Banner extends React.Component<BannerProps> {}
}

declare module '@atlaskit/button' {
  type AnalyticsPayload = {
    action: string;
    [key: string]: any;
  }

  type AnalyticsPayloadUpdater = (
    updater: (AnalyticsPayload | ((payload: AnalyticsPayload) => AnalyticsPayload)),
  ) => UIAnalyticsEvent;

  interface UIAnalyticsEvent {
    fire: (channel?: string) => void;
    update: AnalyticsPayloadUpdater;
  }

  type ButtonAppearances = "default" | "danger" | "link" | "primary" | "subtle" | "subtle-link" | "warning" | "help";

  interface PropTypes {
    appearance?: ButtonAppearances;
    ariaExpanded?: boolean;
    ariaHaspopup?: boolean;
    className?: string;
    isSelected?: boolean;
    isDisabled?: boolean;
    iconBefore?: React.ReactNode;
    iconAfter?: React.ReactNode;
    onClick?(e: React.MouseEvent<HTMLElement>, analyticsEvent: UIAnalyticsEvent): void;
    spacing?: "compact" | "default" | "none";
    href?: string;
    shouldFitContainer?: boolean;
    form?: string;
    type?: "button" | "submit";
  }

  interface ButtonGroupProps {
    appearance?: ButtonAppearances,
  }

  export default class Button extends React.Component<PropTypes> { }
  export class ButtonGroup extends React.Component<PropTypes> { }
}

declare module '@atlaskit/dropdown-menu' {
  import { PropTypes as ButtonPropTypes } from '@atlaskit/button';

  type PositionType =
    | 'top left'
    | 'top center'
    | 'top right'
    | 'right top'
    | 'right middle'
    | 'right bottom'
    | 'bottom left'
    | 'bottom center'
    | 'bottom right'
    | 'left top'
    | 'left middle'
    | 'left bottom';

  interface DropdownMenuPropTypes {
    position?: PositionType;
    /**
    * Controls the type of trigger to be used for the dropdown menu. The default trigger allows you to supply
    * your own trigger component. Setting this prop to button will render a Button component with an 'expand' icon,
    * and the trigger prop contents inside the button.
    */
    triggerType?: 'default' | 'button';
    trigger?: React.ReactNode;
    /**
    * Props to pass through to the trigger button. See @atlaskit/button for allowed props.
    */
    triggerButtonProps?: ButtonPropTypes;
    children?: any;
    onOpenChange?: (data: { isOpen: boolean }) => void;
  }
  export default class DropdownMenu extends React.Component<DropdownMenuPropTypes> { }

  interface DropdownItemGroupPropTypes {
    title?: string;
    children?: any;
  }

  export class DropdownItemGroup extends React.Component<DropdownItemGroupPropTypes> { }

  interface DropdownItemPropTypes {
    href?: string;
    children: any;
    onClick?: () => void;
    target?: string;
  }

  export class DropdownItem extends React.Component<DropdownItemPropTypes> { }
}

declare module '@atlaskit/spinner' {
  export type SpinnerSize = 'small' | 'medium' | 'large' | 'xlarge' | number;

  interface PropTypes {
    delay?: number;
    invertColor?: boolean;
    onComplete?(): void;
    size?: SpinnerSize;
    isCompleting?: boolean;
  }
  export default class Spinner extends React.Component<PropTypes> { }
}

declare module '@atlaskit/theme' {
  const colors: {
    [key: string]: string;
  };
  const borderRadius: () => number;
  const fontFamily: () => string;
  const gridSize: () => number;
  const layers: {
    navigation: () => number;
  };
  const typography: {
    [key: string]: () => string;
  };

  type ThemeModes = 'light' | 'dark';
  type AtlaskitThemeProviderProps = {
    mode?: ThemeModes;
  };

  class AtlaskitThemeProvider extends React.Component<AtlaskitThemeProviderProps> {}
}

declare module '@atlaskit/toggle' {
  interface StatelessProps {
    /**
    * Whether the toggle is disabled or not. This will prevent any interaction with the user
    */
    isDisabled?: boolean;
    /**
    * Label to be set for accessibility
    */
    label?: string;
    /**
    * Descriptive name for value property to be submitted in a form
    */
    name?: string;
    /**
    * The value to be submitted in a form.
    */
    value?: string;
    /**
    * Handler to be called when toggle is unfocused
    */
    onBlur?: () => void;
    /**
    * Handler to be called when native 'change' event happens internally.
    */
    onChange?: () => void;
    /**
    * Handler to be called when toggle is focused.
    */
    onFocus?: () => void;
    /**
    * Defines the size of the toggle.
    */
    size?: 'regular' | 'large';
    /**
    * Whether the toggle is checked or not
    */
    isChecked?: boolean;
  }

  export class ToggleStateless extends React.Component<StatelessProps> { }
}

declare module '@atlaskit/droplist' {
  interface PropTypes {
    appearance?: 'default' | 'tall';
    boundariesElement?: 'viewport' | 'window' | 'scrollParent';
    isLoading?: boolean;
    isOpen?: boolean;
    onClick?(): void;
    onKeyDown?(): void;
    onOpenChange?(): void;
    shouldFitContainer?: boolean;
    shouldFlip?: boolean;
    maxHeight?: number;
    trigger?: JSX.Element;
    position?: string;
  }

  export default class DropList extends React.Component<PropTypes> { }

  export class ItemGroup extends React.Component {}

  interface ElementProps {
    href?: string;
    isActive?: boolean;
    isChecked?: boolean;
    isDisabled?: boolean;
    isFocused?: boolean;
    isHidden?: boolean;
    isPrimary?: boolean;
    isSelected?: boolean;
    title?: string | null;
    type?: 'link' | 'radio' | 'checkbox' | 'option';
  }

  interface ItemProps {
    appearance?: 'default' | 'primary';
    description?: string;
    elemBefore?: JSX.Element;
    elemAfter?: JSX.Element;
    href?: string | null;
    isActive?: boolean;
    isChecked?: boolean;
    isDisabled?: boolean;
    isFocused?: boolean;
    isHidden?: boolean;
    isPrimary?: boolean;
    isSelected?: boolean;
    tooltipDescription?: string | null;
    tooltipPosition?: "top" | "bottom" | "left" | "right";
    onActivate?(): void;
  }

  export class Item extends React.Component<ItemProps> {}
}

interface IconPropTypes {
  label: string;
  primaryColor?: string;
  secondaryColor?: string;
  onClick?(): void;
  size?: "small" | "medium" | "large" | "xlarge";
  onClick?(): void;
}

declare module '@atlaskit/icon/glyph/chevron-down' {
  export default class ChevronDownIcon extends React.Component<IconPropTypes> {}
}

declare module '@atlaskit/icon/glyph/add' {
  export default class AddIcon extends React.Component<IconPropTypes> {}
}

declare module '@atlaskit/icon/glyph/cross-circle' {
  export default class CrossCircleIcon extends React.Component<IconPropTypes> {}
}

declare module '@atlaskit/icon/glyph/calendar' {
  export default class CalendarIcon extends React.Component<IconPropTypes> {}
}

declare module '@atlaskit/field-text' {
  interface TextFieldStatelessProps {
    /**
    * Standard HTML input autocomplete attribute.
    */
    autoComplete?: 'on' | 'off';
    /**
    * Standard HTML input form attribute. This is useful if the input cannot be included directly inside a form.
    */
    form?: string;
    /**
    * Standard HTML input pattern attribute, used for validating using a regular expression.
    */
    pattern?: string;
    /**
    * Set whether the fields should expand to fill available horizontal space.
    */
    compact?: boolean;
    /**
    * Type value to be passed to the html input.
    */
    type?: string;
    /**
    * Sets the field as uneditable, with a changed hover state.
    */
    disabled?: boolean;
    /**
    * If true, prevents the value of the input from being edited.
    */
    isReadOnly?: boolean;
    /**
    * Add asterisk to label. Set required for form that the field is part of.
    */
    required?: boolean;
    /**
    * Sets styling to indicate that the input is invalid.
    */
    isInvalid?: boolean;
    /**
    * Label to be displayed above the input.
    */
    label?: string;
    /**
    * Name value to be passed to the html input.
    */
    name?: string;
    /**
    * Standard input min attribute, to be used with type="number"
    */
    min?: number;
    /**
    * Standard input max attribute, to be used with type="number"
    */
    max?: number;
    /**
    * Text to display in the input if the input is empty.
    */
    placeholder?: string;
    /**
    * The value of the input.
    */
    value: string | number;
    /**
    * Handler to be called when the input loses focus.
    */
    onBlur?: (event: Event) => void;
    /**
    * Handler to be called when the input changes.
    */
    onChange?(event: React.SyntheticEvent<HTMLInputElement>): void;
    /**
    * Handler to be called when the input receives focus.
    */
    onFocus?: () => void;
    /**
    * Standard input onkeydown event.
    */
    onKeyDown?: (event: KeyboardEvent) => void;
    /**
    * Standard input onkeypress event.
    */
    onKeyPress?: (event: KeyboardEvent) => void;
    /**
    * Standard input onkeyup event.
    */
    onKeyUp?: (event: KeyboardEvent) => void;
    /**
    * Id value to be passed to the html input.
    */
    id?: string;
    /**
    * Sets whether to show or hide the label.
    */
    isLabelHidden?: boolean;
    /**
    * Provided component is rendered inside a modal dialogue when the field is selected.
    */
    invalidMessage?: JSX.Element;
    /**
    * Ensure the input fits in to its containing element.
    */
    shouldFitContainer?: boolean;
    /**
    * Sets whether to apply spell checking to the content.
    */
    isSpellCheckEnabled?: boolean;
    /**
    * Sets whether the component should be automatically focused on component render.
    */
    autoFocus?: boolean;
    /**
    * Set the maximum length that the entered text can be.
    */
    maxLength?: number;
    /**
    * Hide the validation message and style. This is used by to disable Validation display handling by FieldBase
    */
    isValidationHidden?: boolean;
  }

  export class FieldTextStateless extends React.Component<TextFieldStatelessProps> {}

  interface TextFieldProps {
    autoComplete?: 'on' | 'off';
    form?: string;
    pattern?: string;
    compact?: boolean;
    type?: string;
    disabled?: boolean;
    isReadOnly?: boolean;
    required?: boolean;
    isInvalid?: boolean;
    invalidMessage?: JSX.Element | string;
    label?: JSX.Element | string;
    name?: string;
    min?: number;
    max?: number;
    placeholder?: string;
    value?: string | number;
    onBlur?(): void;
    onChange?(event: React.SyntheticEvent<HTMLInputElement>): void;
    onFocus?(): void;
    onKeyDown?(): void;
    onKeyPress?(): void;
    onKeyUp?(): void;
    id?: string;
    isLabelHidden?: boolean;
    shouldFitContainer?: boolean;
    isSpellCheckEnabled?: boolean;
    autoFocus?: boolean;
    maxLength?: number;
  }

  export default class TextField extends React.Component<TextFieldProps> {}
}

declare module '@atlaskit/field-text-area' {
  interface FieldTextAreaStatelessProps {
    /** Set whether the fields should expand to fill available horizontal space. */
    compact?: boolean;
    /** Sets the field as uneditable, with a changed hover state. */
    disabled?: boolean;
    /** If true, prevents the value of the input from being edited. */
    isReadOnly?: boolean;
    /** Add asterisk to label. Set required for form that the field is part of. */
    required?: boolean;
    /** Sets styling to indicate that the input is invalid. */
    isInvalid?: boolean;
    /** Label to be displayed above the input. */
    label: string;
    /** Name value to be passed to the html input. */
    name?: string;
    /** Text to display in the input if the input is empty. */
    placeholder?: string;
    /** The value of the input. */
    value?: string | number;
    /** Handler to be called when the input is blurred */
    onBlur?: (event: React.SyntheticEvent<HTMLTextAreaElement>) => void;
    /** Handler to be called when the input changes. */
    onChange?: (event: React.SyntheticEvent<HTMLTextAreaElement>) => void;
    /** Handler to be called when the input is focused */
    onFocus?: (event: React.SyntheticEvent<HTMLTextAreaElement>) => void;
    /** Id value to be passed to the html input. */
    id?: string;
    /** Sets whether to show or hide the label. */
    isLabelHidden?: boolean;
    /** Provided component is rendered inside a modal dialogue when the field is
     selected. */
    invalidMessage?: JSX.Element;
    /** Ensure the input fits in to its containing element. If the field is still
     resizable, it will not be hotizontally resizable. */
    shouldFitContainer?: boolean;
    /** Sets whether to apply spell checking to the content. */
    isSpellCheckEnabled?: boolean;
    /** Sets whether the component should be automatically focused on component
     render. */
    autoFocus?: boolean;
    /** Set the maximum length that the entered text can be. */
    maxLength?: number;
    /** The minimum number of rows of text to display */
    minimumRows?: number;
    /** Enables the resizing of the textarea (in both directions, or restricted to one axis) */
    enableResize?: boolean | 'horizontal' | 'vertical';
    /** Type of field */
    type?: string; //eslint-disable-line react/no-unused-prop-types
    /** Hide the validation message and style. This is used by <Field> to disable Validation display handling by FieldBase
     */
    isValidationHidden?: boolean;
  }

  export class FieldTextAreaStateless extends React.Component<FieldTextAreaStatelessProps> {}

  interface FieldTextAreaProps {
    compact?: boolean;
    disabled?: boolean;
    isReadOnly?: boolean;
    required?: boolean;
    isInvalid?: boolean;
    label?: string;
    name?: string;
    placeholder?: string;
    value?: string | number;
    onChange?: (event: React.SyntheticEvent<HTMLTextAreaElement>) => void;
    id?: string;
    isLabelHidden?: boolean;
    invalidMessage?: JSX.Element;
    shouldFitContainer?: boolean;
    isSpellCheckEnabled?: boolean;
    autoFocus?: boolean;
    maxLength?: number;
    minimumRows?: number;
    enableResize?: boolean;
  }

  export default class FieldTextArea extends React.Component<FieldTextAreaProps> {}
}

declare module '@atlaskit/inline-dialog' {
  type Placement =
    | 'auto-start'
    | 'auto'
    | 'auto-end'
    | 'top-start'
    | 'top'
    | 'top-end'
    | 'right-start'
    | 'right'
    | 'right-end'
    | 'bottom-end'
    | 'bottom'
    | 'bottom-start'
    | 'left-end'
    | 'left'
    | 'left-start';

  interface InlineDialogProps {
    /** The elements that the InlineDialog will be positioned relative to. */
    children?: JSX.Element;
    /** The elements to be displayed within the InlineDialog. */
    content?: JSX.Element;
    /** Sets whether to show or hide the dialog. */
    isOpen?: boolean;
    /** Function called when you lose focus on the object. */
    onContentBlur?: () => void;
    /** Function called when you click on the open dialog. */
    onContentClick?: () => void;
    /** Function called when you focus on the open dialog. */
    onContentFocus?: () => void;
    /** Function called when the dialog is open and a click occurs anywhere outside
     the dialog. */
    onClose?: (data?: { isOpen: false, event: any }) => void;
    /** Where the dialog should appear, relative to the contents of the children. */
    placement?: Placement;
  }
  export default class InlineDialog extends React.Component<InlineDialogProps> {}
}

declare module '@atlaskit/logo' {
    export interface Props {
        /** The size of the icon, uses the same sizing scheme as in @atlaskit/icon */
        size?: 'xsmall' | 'small' | 'medium' | 'large' | 'xlarge',
        /** CSS color to be applied to the wordmark portion of the logo SVG */
        textColor?: string,
        /** CSS color to be applied to the non-gradient icon portion of the logo SVG */
        iconColor?: string,
        /** CSS color to start the gradient/shadow on the icon */
        iconGradientStart?: string,
        /** CSS color to end the gradient/shadow on the icon. Should usually match iconColor to avoid
         * rendering issues in some browsers such as Safari. */
        iconGradientStop?: string,
        /** Accessible text to be used for screen readers */
        label?: string,
    }

    export class AtlassianLogo extends React.Component<Props> {}
    export class AtlassianIcon extends React.Component<Props> {}
    export class AtlassianWordmark extends React.Component<Props> {}
    export class BitbucketLogo extends React.Component<Props> {}
    export class BitbucketIcon extends React.Component<Props> {}
    export class BitbucketWordmark extends React.Component<Props> {}
    export class ConfluenceLogo extends React.Component<Props> {}
    export class ConfluenceIcon extends React.Component<Props> {}
    export class ConfluenceWordmark extends React.Component<Props> {}
    export class HipchatLogo extends React.Component<Props> {}
    export class HipchatIcon extends React.Component<Props> {}
    export class HipchatWordmark extends React.Component<Props> {}
    export class JiraCoreLogo extends React.Component<Props> {}
    export class JiraCoreIcon extends React.Component<Props> {}
    export class JiraCoreWordmark extends React.Component<Props> {}
    export class JiraLogo extends React.Component<Props> {}
    export class JiraIcon extends React.Component<Props> {}
    export class JiraWordmark extends React.Component<Props> {}
    export class JiraServiceDeskLogo extends React.Component<Props> {}
    export class JiraServiceDeskIcon extends React.Component<Props> {}
    export class JiraServiceDeskWordmark extends React.Component<Props> {}
    export class JiraSoftwareLogo extends React.Component<Props> {}
    export class JiraSoftwareIcon extends React.Component<Props> {}
    export class JiraSoftwareWordmark extends React.Component<Props> {}
    export class StatuspageLogo extends React.Component<Props> {}
    export class StatuspageIcon extends React.Component<Props> {}
    export class StatuspageWordmark extends React.Component<Props> {}
    export class StrideLogo extends React.Component<Props> {}
    export class StrideIcon extends React.Component<Props> {}
    export class StrideWordmark extends React.Component<Props> {}
}

declare module '@atlaskit/select' {
  type MenuPlacement = 'auto' | 'bottom' | 'top';

  type Option = {
    label: string;
    value: string;
  };

  type GroupOption = {
    label: string;
    options: Option[];
  };

  type OptionTypes = Option | GroupOption;

  type ValueType = OptionTypes | OptionTypes[] | null;

  type Action = "select-option" | "deselect-option" | "remove-value" | "pop-value" | "set-value" | "clear" | "create-option";

  // Copied from https://github.com/JedWatson/react-select/blob/v2/src/Select.js#L55
  interface SelectProps {
    /* HTML ID(s) of element(s) that should be used to describe this input (for assistive tech) */
    'aria-describedby'?: string,
    /* Aria label (for assistive tech) */
    'aria-label'?: string,
    /* HTML ID of an element that should be used as the label (for assistive tech) */
    'aria-labelledby'?: string,
    /* Focus the control when it is mounted */
    autoFocus?: boolean,
    /* Remove the currently focused option when the user presses backspace */
    backspaceRemovesValue?: boolean,
    /* Remove focus from the input when the user selects an option (handy for dismissing the keyboard on touch devices) */
    blurInputOnSelect?: boolean,
    /* When the user reaches the top/bottom of the menu, prevent scroll on the scroll-parent  */
    captureMenuScroll?: boolean,
    /* Close the select menu when the user selects an option */
    closeMenuOnSelect?: boolean,
    /* Custom components to use */
    components?: {},
    /* Delimiter used to join multiple values into a single HTML Input value */
    delimiter?: string,
    /* Clear all values when the user presses escape AND the menu is closed */
    escapeClearsValue?: boolean,
    /* Hide the selected option from the menu */
    hideSelectedOptions?: boolean,
    /* The value of the search input */
    inputValue?: string,
    /* Define an id prefix for the select components e.g. {your-id}-value */
    instanceId?: number | string,
    /* Is the select value clearable */
    isClearable?: boolean,
    /* Is the select disabled */
    isDisabled?: boolean,
    /* Is the select in a state of loading (async) */
    isLoading?: boolean,
    /* Support multiple selected options */
    isMulti?: boolean,
    /* Is the select direction right-to-left */
    isRtl?: boolean,
    /* Whether to enable search functionality */
    isSearchable?: boolean,
    /* Async: Text to display when loading options */
    loadingMessage?: (message: { inputValue: string }) => string,
    /* Minimum height of the menu before flipping */
    minMenuHeight?: number,
    /* Maximum height of the menu before scrolling */
    maxMenuHeight?: number,
    /* Maximum height of the value container before scrolling */
    maxValueHeight?: number,
    /* Whether the menu is open */
    menuIsOpen?: boolean,
    /*
    Default placement of the menu in relation to the control. 'auto' will flip
    when there isn't enough space below the control.
    */
    menuPlacement?: MenuPlacement,
    /* Name of the HTML Input (optional - without this, no input will be rendered) */
    name?: string,
    /* Text to display when there are no options */
    noOptionsMessage?: (input: { inputValue: string }) => string,
    /* Handle blur events on the control */
    onBlur?(): void,
    /* Handle change events on the select */
    onChange?(option: Option | null, action: { action: Action }): void,
    /* Handle focus events on the control */
    onFocus?(): void,
    /* Handle change events on the input */
    onInputChange?(event: string): void,
    /* Handle key down events on the select */
    onKeyDown?(): void,
    /* Handle the menu opening */
    onMenuOpen?: (e: Option | null) => void,
    /* Handle the menu closing */
    onMenuClose?: () => void,
    /* Array of options that populate the select menu */
    options?: OptionTypes[],
    /* Number of options to jump in menu when page{up|down} keys are used */
    pageSize?: number,
    /* Placeholder text for the select value */
    placeholder?: string,
    /* Status to relay to screen readers */
    screenReaderStatus?: (status: { count: number }) => string,
    /* Whether the menu should be scrolled into view when it opens */
    scrollMenuIntoView?: boolean,
    /* Style modifier methods */
    styles?: {},
    /* Select the currently focused option when the user presses tab */
    tabSelectsValue?: boolean,
    /* The value of the select; reflected by the selected option */
    value?: ValueType
  }

  export default class Select extends React.Component<SelectProps> {}

  type CheckboxSelectProps = Pick<SelectProps,
    'aria-describedby' |
    'aria-label' |
    'aria-labelledby' |
    'autoFocus' |
    'backspaceRemovesValue' |
    'blurInputOnSelect' |
    'captureMenuScroll' |
    'closeMenuOnSelect' |
    'components' |
    'delimiter' |
    'escapeClearsValue' |
    'hideSelectedOptions' |
    'inputValue' |
    'instanceId' |
    'isClearable' |
    'isDisabled' |
    'isLoading' |
    'isMulti' |
    'isRtl' |
    'isSearchable' |
    'loadingMessage' |
    'minMenuHeight' |
    'maxMenuHeight' |
    'maxValueHeight' |
    'menuIsOpen' |
    'menuPlacement' |
    'name' |
    'noOptionsMessage' |
    'onBlur' |
    'onChange' |
    'onFocus' |
    'onInputChange' |
    'onKeyDown' |
    'onMenuOpen' |
    'onMenuClose' |
    'options' |
    'pageSize' |
    'placeholder' |
    'screenReaderStatus' |
    'scrollMenuIntoView' |
    'styles' |
    'tabSelectsValue' |
    'value'
  >

  export class CheckboxSelect extends React.Component<CheckboxSelectProps> {}
}

declare module '@atlaskit/datetime-picker' {
  interface DateTimePickerProps {
    // Whether or not to auto-focus the field.
    autoFocus?: boolean;

    // Default for focused.
    defaultFocused?: string;

    // Default for isOpen.
    defaultIsOpen?: boolean;

    // Default for times.
    defaultTimes?: Array<string>;

    // Default for value.
    defaultValue?: string | string[];

    // An array of ISO dates that should be disabled on the calendar.
    disabled?: Array<string>;

    // Whether or not the field is disabled.
    isDisabled?: boolean;

    // Whether or not the dropdown is open.
    isOpen?: boolean;

    // The time in the dropdown that should be focused.
    focused?: string;

    // Called when the value changes. The first argument is an ISO date and the second is an ISO time.
    onChange: (date: string, time: string) => void;

    // The times to show in the dropdown.
    times?: Array<string>;

    // The ISO time that should be used as the input value.
    value?: string;

    // The width of the field.
    width?: number;
  }

  export class DateTimePicker extends React.Component<DateTimePickerProps> {}

  interface DatePickerProps {
    // Whether or not to auto-focus the field.
    autoFocus?: boolean;

    // Default for isOpen.
    defaultIsOpen?: boolean;

    // Default for value.
    defaultValue?: string;

    // An array of ISO dates that should be disabled on the calendar.
    disabled?: string[];

    // Whether or not the field is disabled.
    isDisabled?: boolean;

    // Whether or not the dropdown is open.
    isOpen?: boolean;

    // Called when the value changes. The only argument is an ISO time.
    onChange?: (date: string) => void;

    // The ISO time that should be used as the input value.
    value?: string;

    // The width of the field.
    width?: number;
  }

  export class DatePicker extends React.Component<DatePickerProps> {}
}

declare module '@atlaskit/field-base' {
  interface LabelProps {
    label: string;
  }

  export class Label extends React.Component<LabelProps> {}
}

declare module '@atlaskit/dynamic-table' {
  export interface DynamicTableHeadCell {
    key: string | number;
    content: JSX.Element | string;
    isSortable?: boolean;
    width?: number;
    shouldTruncate?: boolean;
  }

  export interface DynamicTableHead {
    cells: DynamicTableHeadCell[];
  }

  export interface DynamicTableCell {
    key: string | number;
    content: JSX.Element;
  }

  export interface DynamicTableRow {
    cells: DynamicTableCell[];
    key: string;
  }

  export interface RankStart {
    index: number;
    key: string;
  }

  export interface DynamicTableProps {
    defaultPage?: number;
    defaultSortKey?: string;
    defaultSortOrder?: 'ASC' | 'DESC';
    caption?: JSX.Element;
    head?: DynamicTableHead;
    rows?: DynamicTableRow[];
    emptyView?: JSX.Element;
    loadingSpinnerSize?: 'small' | 'large';
    isLoading?: boolean;
    isFixedSize?: boolean;
    rowsPerPage?: number;
    onSetPage?(): void;
    onSort?(): void;
    page?: number;
    sortKey?: string;
    sortOrder?: 'ASC' | 'DESC';
    isRankable?: boolean;
    isRankingDisabled?: boolean;
    onRankStart?: (s: RankStart) => void;
  }

  export default class DynamicTable extends React.Component<DynamicTableProps> {}
}

declare module '@atlaskit/empty-state' {
  interface EmptyStateProps {
    header: string;
    description?: string;
    size?: 'wide' | 'narrow';
    imageUrl?: string;
    maxImageWidth?: number;
    maxImageHeight?: number;
    primaryAction?: JSX.Element;
    secondaryAction?: JSX.Element;
    tertiaryAction?: JSX.Element;
    isLoading?: boolean;
  }

  export default class EmptyState extends React.Component<EmptyStateProps> {}
}

declare module '@atlaskit/navigation' {
  type OneOrMoreElements = JSX.Element | JSX.Element[];

  interface ResizeObj {
    width: number;
    isOpen: boolean;
  }

  type IconAppearance = 'square' | 'round';

  interface NavigationProps {
    /** Elements to be displayed in the ContainerNavigationComponent */
    children?: OneOrMoreElements,
    /** Theme object to be used to color the navigation container. */
    //containerTheme?: Provided,
    /** Component(s) to be rendered as the header of the container.  */
    containerHeaderComponent?: () => OneOrMoreElements,
    /** Standard React ref for the container navigation scrollable element. */
    containerScrollRef?: (ref: React.Ref<any>) => void,
    /** Location to pass in an array of drawers (AkCreateDrawer, AkSearchDrawer, AkCustomDrawer)
    to be rendered. There is no decoration done to the components passed in here. */
    drawers: JSX.Element[],
    /** Theme object to be used to color the global container. */
    //globalTheme?: Provided,
    /** Icon to be used as the 'create' icon. onCreateDrawerOpen is called when it
    is clicked. */
    globalCreateIcon?: JSX.Element,
    /** Icon to be displayed at the top of the GlobalNavigation. This is wrapped in
    the linkComponent. */
    globalPrimaryIcon?: JSX.Element,
    /** Appearance of globalPrimaryIcon for shape styling of drop shadows */
    globalPrimaryIconAppearance: IconAppearance,
    /** Link to be passed to the linkComponent that wraps the globalCreateIcon. */
    globalPrimaryItemHref?: string,
    /** Icon to be used as the 'create' icon. onSearchDrawerOpen is called when it
    is clicked. */
    globalSearchIcon?: JSX.Element,
    /** A list of nodes to be rendered as the global primary actions. They appear
    directly underneath the global primary icon. This must not exceed three nodes */
    globalPrimaryActions?: JSX.Element[],
    /** An array of elements to be displayed at the bottom of the global component.
    These should be icons or other small elements. There must be no more than five.
    Secondary Actions will not be visible when nav is collapsed. */
    globalSecondaryActions?: JSX.Element[],
    /** Whether to display a scroll hint shadow at the top of the ContainerNavigation
    * wrapper. */
    hasScrollHintTop?: boolean,
    /** Set whether collapse should be allowed. If false, the nav cannot be dragged
    to be smaller. */
    isCollapsible?: boolean,
    /** Set whether the nav is collapsed or not. Note that this is never controlled
    internally as state, so if it is collapsible, you need to manually listen to onResize
    to determine when to change this if you are letting users manually collapse the
    nav. */
    isOpen?: boolean,
    /** Sets whether to disable all resize prompts. */
    isResizeable?: boolean,
    /** Causes leftmost navigation section to be slightly wider to accommodate macOS buttons. */
    isElectronMac?: boolean,
    /** A component to be used as a link. By Default this is an anchor. when a href
    is passed to it, and otherwise is a button. */
    linkComponent?: React.ComponentType<any>,
    /** Function called at the end of a resize event. It is called with an object
    containing a width and an isOpen. These can be used to update the props of Navigation. */
    onResize?: (resizeState: ResizeObj) => void,
    /** Function to be called when a resize event starts. */
    onResizeStart?: () => void,
    /** Function called when the globalCreateIcon is clicked. */
    onCreateDrawerOpen?: () => void,
    /** Function called when the globalSearchIcon is clicked. */
    onSearchDrawerOpen?: () => void,
    /** Function called when a collapse/expand starts */
    onToggleStart?: () => void,
    /** Function called when a collapse/expand finishes */
    onToggleEnd?: () => void,
    /** The offset at the top of the page before the navigation begins. This allows
    absolute items such as a banner to be placed above nav, without lower nav items
    being pushed off the screen. **DO NOT** use this outside of this use-case. Changes
    are animated. The string is any valid css height value */
    topOffset?: number,
    /** Width of the navigation. Width cannot be reduced below the minimum, and the
    collapsed with will be respected above the provided width. */
    width?: number,
  }

  export default class Navigation extends React.Component<NavigationProps> {}

  interface AkGlobalItemProps {
    /** Standard aria-haspopup prop */
    'aria-haspopup'?: string, // eslint-disable-line react/no-unused-prop-types
    /** Element to be rendered inside the item. Should be an atlaskit icon. */
    children?: OneOrMoreElements,
    /** href to pass to linkComponent.  */
    href?: string,
    /** Causes the item to appear with a persistent selected background state. */
    isSelected?: boolean,
    /** Component to be used to create the link in the global item. A default
    component is used if none is provided. */
    linkComponent?: React.ComponentType<any>,
    /** Standard onClick event */
    onClick?: (event: Event, data?: {}) => void,
    onMouseDown?: (event: MouseEvent) => void,
    /** ARIA role to apply to the global item. */
    role?: string,
    /** Set the size of the item's content.  */
    size?: 'small' | 'medium' | 'large',
    /** Appearance of item for custom styling (square or round) */
    appearance?: IconAppearance,
  }

  export class AkGlobalItem extends React.Component<AkGlobalItemProps> {}

  export class AkNavigationItemGroup extends React.Component {}

  interface AkNavigationItemProps {
    action?: JSX.Element,
    /** Text to appear to the right of the text. It has a lower font-weight. */
    caption?: string,
    /** Drag and drop props provided by react-beautiful-dnd. Please do not use
    * this unless using react-beautiful-dnd */
    //dnd?: DnDType,
    /** Location to link out to on click. This is passed down to the custom link
    component if one is provided. */
    href?: string,
    /** Target frame for item `href` link to be aimed at. */
    target?: string,
    /** React element to appear to the left of the text. This should be an
    @atlaskit/icon component. */
    icon?: Node,
    /** Element displayed to the right of the item. The dropIcon should generally be
    an appropriate @atlaskit icon, such as the ExpandIcon. */
    dropIcon?: Node,
    /** Makes the navigation item appear with reduced padding and font size. */
    isCompact?: boolean,
    /** Used to apply correct dragging styles when also using react-beautiful-dnd. */
    isDragging?: boolean,
    /** Set whether the item should be highlighted as selected. Selected items have
    a different background color. */
    isSelected?: boolean,
    /** Set whether the item should be used to trigger a dropdown. If this is true,
    The href property will be disabled. */
    isDropdownTrigger?: boolean,
    /** Component to be used as link, if default link component does not suit, such
    as if you are using a different router. Component is passed a href prop, and the content
    of the title as children. Any custom link component must accept a className prop so that
    it can be styled. */
    linkComponent?: React.ComponentType<any>,
    /** Function to be called on click. This is passed down to a custom link component,
    if one is provided.  */
    onClick?: (event?: MouseEvent) => void,
    /** Function to be called on click. This is passed down to a custom link component,
    if one is provided.  */
    onKeyDown?: (e: KeyboardEvent) => void,
    /** Standard onmouseenter event */
    onMouseEnter?: (e: MouseEvent) => void,
    /** Standard onmouseleave event */
    onMouseLeave?: (e: MouseEvent) => void,
    /** Text to be shown alongside the main `text`. */
    subText?: string,
    /** Main text to be displayed as the item. Accepts a react component but in most
    cases this should just be a string. */
    text?: JSX.Element | string,
    /** React component to be placed to the right of the main text. */
    textAfter?: Node,
    /** Whether the Item should attempt to gain browser focus when mounted */
    autoFocus?: boolean,
  }

  export class AkNavigationItem extends React.Component<AkNavigationItemProps> {}

  interface AkContainerTitleProps {
    /** Location to link out to on click. This is passed down to the custom link
    component if one is provided. */
    href?: string,
    /** React element to appear to the left of the text. This should be an
    @atlaskit/icon component. */
    icon?: JSX.Element,
    /** Component to be used as link, if default link component does not suit, such
    as if you are using a different router. Component is passed a href prop, and the content
    of the title as children. Any custom link component must accept a className prop so that
    it can be styled. */
    linkComponent?: React.ComponentType<any>,
    /** Function to be called on click. This is passed down to a custom link component,
    if one is provided.  */
    onClick?: (event?: MouseEvent) => void,
    /** Function to be called on click. This is passed down to a custom link component,
    if one is provided.  */
    onKeyDown?: (e: KeyboardEvent) => void,
    /** Standard onmouseenter event */
    onMouseEnter?: (e: MouseEvent) => void,
    /** Standard onmouseleave event */
    onMouseLeave?: (e: MouseEvent) => void,
    /** Text to be shown alongside the main `text`. */
    subText?: string,
    /** Main text to be displayed as the item. Accepts a react component but in most
    cases this should just be a string. */
    text?: JSX.Element | string,
  }

  export class AkContainerTitle extends React.Component<AkContainerTitleProps> {}
}

declare module '@atlaskit/tooltip' {
  type PositionType = 'bottom' | 'left' | 'right' | 'top';

  interface TooltipProps {
    /** A single element, either Component or DOM node */
    children: JSX.Element[] | JSX.Element,
    /** The content of the tooltip */
    content: JSX.Element | string,
    /** Extend `TooltipPrimitive` to create you own tooptip and pass it as component */
    component?: React.ComponentType<{ innerRef: (element: HTMLElement) => void }>,
    /** Hide the tooltip when the element is clicked */
    hideTooltipOnClick?: boolean,
    /** Function to be called when a mouse leaves the target */
    onMouseOut?: (event: MouseEvent) => void,
    /** Function to be called when a mouse enters the target */
    onMouseOver?: (event: MouseEvent) => void,
    /** Where the tooltip should appear relative to its target */
    position?: PositionType,
    /** Replace the wrapping element */
    tag?: string,
    /** Show only one line of text, and truncate when too long */
    truncate?: boolean,
  }

  export default class Tooltip extends React.Component<TooltipProps> {}
}

declare module '@atlaskit/tabs' {
  import { ComponentType, KeyboardEvent, MouseEvent,  } from 'react';

  interface TabItemElementProps {
    'aria-posinset'?: number;
    'aria-selected'?: boolean;
    'aria-setsize'?: number;
    onClick?: () => void;
    onKeyDown?: (e: KeyboardEvent<HTMLAnchorElement>) => void;
    onMouseDown?: (e: MouseEvent<HTMLAnchorElement>) => void;
    role?: string;
    tabIndex?: number | string;
  }

  type TabItemInnerRef = (ref: HTMLElement) => void;

  interface TabData {
    label: JSX.Element | string;
    content: JSX.Element | null;
    [key: string]: any;
  }

  export interface TabItemComponentProvided {
    data: TabData;
    elementProps: TabItemElementProps;
    innerRef: TabItemInnerRef;
    isSelected: boolean;
  }

  type TabItemType = ComponentType<TabItemComponentProvided>;

  type OnSelectCallback = (selected: TabData, selectedIndex: number) => void;

  interface TabsProps {
    components?: {
      Item?: TabItemType,
    },
    isSelectedTest?: (selected: any, tab: {}, index: number) => boolean;
    tabs: TabData[];
    selected?: TabData | number;
    defaultSelected?: TabData | number;
    onSelect?: OnSelectCallback;
  }

  export default class Tabs extends React.Component<TabsProps> {}

  export class TabItem extends React.Component<TabItemComponentProvided> {}
}

declare module '@atlaskit/lozenge' {
  type LozengeAppearance = "default" | "success" | "removed" | "inprogress" | "new" | "moved";

  interface LozengeProps {
    isBold?: boolean;
    appearance?: LozengeAppearance;
    children?: JSX.Element | string;
  }

  export default class Lozenge extends React.Component<LozengeProps> {}
}

declare module '@atlaskit/code' {
  interface AkCodeProps {
    text: string;
    language?: string;
  }

  export class AkCode extends React.Component<AkCodeProps> {}

  export class AkCodeBlock extends React.Component<AkCodeProps> {}
}

declare module '@atlaskit/modal-dialog' {
  type ModalDialogWidth = "small" | "medium" | "large" | "x-large";

  interface ModalProps {
    /**
    * The modal title; rendered in the header.
    */
    heading?: string;
    header?: JSX.Element | (() => JSX.Element);
    /**
    * Component to render the footer of the modal, replaces internal implementation.
    */
    footer?: JSX.Element | (() => JSX.Element);
    appearance?: 'danger' | 'warning';
    onClose?: () => void;
    body?: JSX.Element;
    width?: number | ModalDialogWidth;
    shouldCloseOnOverlayClick?: boolean;
    shouldCloseOnEscapePress?: boolean;
    isHeadingMultilineboolean?: boolean;
    /**
    * Buttons to render in the footer
    */
    actions?: ModalFooterAction[];
  }

  export default class Modal extends React.Component<ModalProps> {}

  interface ModalFooterAction {
    onClick?: () => void;
    text?: string;
  }

  interface ModalFooterProps {
    /** Buttons to render in the footer */
    actions?: ModalFooterAction[];
    /** Appearance of the primary button. Also adds an icon to the heading, if provided. */
    appearance?: 'danger' | 'warning',
    /** Component to render the footer of the moda.l */
    component?: JSX.Element,
    /** Function to close the dialog */
    onClose?: () => void, // TODO This should be a required property but some components have chosen to not include it
    /** Whether or not to display a line above the footer */
    showKeyline?: boolean,
  }

   export class ModalFooter extends React.Component<ModalFooterProps> {}
}

declare module '@atlaskit/breadcrumbs' {
  interface BreadcrumbsStatelessProps {
    isExpanded?: boolean;
    maxItems?: number;
    onExpand: (e: Event) => void;
  }

  export class BreadcrumbsStateless extends React.Component<BreadcrumbsStatelessProps> {}

  interface BreadcrumbsItemProps {
    hasSeparator?: boolean;
    href?: string;
    iconBefore?: React.ReactNode;
    iconAfter?: React.ReactNode;
    onClick?: (e: Event) => void;
    text: string;
    truncationWidth?: number;
    target?: "_blank" | "_parent" | "_self" | "_top";
  }

  export class BreadcrumbsItem extends React.Component<BreadcrumbsItemProps> {}
}

declare module '@atlaskit/page' {
  interface GridProps {
    children?: any;
    spacing?: 'cosy' | 'comfortable' | 'compact';
    layout?: 'fixed' | 'fluid';
  }

  export class Grid extends React.Component<GridProps> {}

  interface GridColumnProps {
    medium?: number;
    children?: React.ReactNode;
  }

  export class GridColumn extends React.Component<GridColumnProps> {}

  interface PageProps {
    banner?: React.ReactNode;
    children?: React.ReactNode;
    isBannerOpen?: boolean;
    navigation?: React.ReactNode;
  }

  export default class Page extends React.Component<PageProps> {}
}

declare module '@atlaskit/inline-edit' {
  type BaseInlineEditProps = {
    /** Label above the input. */
    label: string;
    /** Component to be shown when reading only */
    readView: JSX.Element;
    /** Component to be shown when editing. Should be an @atlaskit/input. */
    editView?: JSX.Element;
    /** Set whether the read view should fit width, most obvious when hovered. */
    isFitContainerWidthReadView?: boolean;
    /** Greys out text and shows spinner. Does not disable input. */
    isWaiting?: boolean;
    /** Sets yellow border with warning symbol at end of input. Removes confirm
     and cancel buttons. */
    isInvalid?: boolean;
    /** Determine whether the label is shown. */
    isLabelHidden?: boolean;
    /** Sets whether the checkmark and cross are displayed in the bottom right fo the field. */
    areActionButtonsHidden?: boolean;
    /** Sets whether the confirm function is called when the input loses focus. */
    isConfirmOnBlurDisabled?: boolean;
    /** Handler called when checkmark is clicked. Also by default
     called when the input loses focus. */
    onConfirm: () => void;
    /** Handler called when the cross is clicked on. */
    onCancel: () => void;
    /** html to pass down to the label htmlFor prop. */
    labelHtmlFor?: string;
    /** Set whether onConfirm is called on pressing enter. */
    shouldConfirmOnEnter?: boolean;
    /** Set whether default stylings should be disabled when editing. */
    disableEditViewFieldBase?: boolean;
    /** Component to be shown in an @atlaskit/inline-dialog when edit view is open. */
    // @see https://ecosystem.atlassian.net/browse/AK-4765
    invalidMessage?: React.ReactNode;
  };

  export default class InlineEdit extends React.Component<BaseInlineEditProps> { }

  export type StatelessInlineEditProps = BaseInlineEditProps & {
    /** Whether the component shows the readView or the editView. */
    isEditing: boolean;
    /** Handler called when the wrapper or the label are clicked. */
    onEditRequested: () => void;
  };

  export class InlineEditStateless extends React.Component<StatelessInlineEditProps> { }
}

declare module '@atlaskit/input' {
  interface InputProps {
    value?: string | number;
    style?: Object;
    isInitiallySelected?: boolean;
    isEditing: boolean;
    onConfirm?: (e: React.SyntheticEvent<HTMLInputElement>) => void;
    onKeyDown?: (e: React.SyntheticEvent<HTMLInputElement>) => void;
    /** This does not seem to exist in the AK flow types, but it's used in all the InlineEdit examples */
    onChange?: (e: React.SyntheticEvent<HTMLInputElement>) => void;
  }

  export default class SingleLineTextInput extends React.Component<InputProps> { }
}

declare module '@atlaskit/flag' {
  type ChildrenType = any;
  type ElementType = any;
  type FunctionType = (...args: Array<any>) => any;
  type MouseEventFunctionType = (event: MouseEvent) => any;
  type GenericEventFunctionType = (event: Event) => any;

  type AppearanceTypes =
    | 'error'
    | 'info'
    | 'normal'
    | 'success'
    | 'warning';

  type ActionsType = Array<{
    content: Node,
    onClick: FunctionType,
  }>;

  type AutoDismissFlagProps = {
    /** Array of clickable actions to be shown at the bottom of the flag. For flags where appearance
     * is 'normal', actions will be shown as links. For all other appearance values, actions will
     * shown as buttons.
     */
    actions?: ActionsType,
    /** Makes the flag appearance bold. Setting this to anything other than 'normal' hides the
     * dismiss button.
     */
    appearance?: AppearanceTypes,
    /** The secondary content shown below the flag title */
    description?: React.ReactNode,
    /** The icon displayed in the top-left of the flag. Should be an instance of `@atlaskit/icon`.
     * Your icon will receive the appropriate default color, which you can override by wrapping the
     * icon in a containing element with CSS `color` set to your preferred icon color.
     */
    icon: ElementType,
    /** A unique identifier used for rendering and onDismissed callbacks. */
    id: number | string,
    /** Private, do not use. */
    isDismissAllowed?: boolean,
    /** Private, do not use. Use the FlagGroup onDismissed handler. */
    onDismissed?: FunctionType,
    /** The bold text shown at the top of the flag. */
    title: string,
  };

  type FlagProps = AutoDismissFlagProps & {
    /** Standard onBlur event, applied to Flag by AutoDismissFlag */
    onBlur?: GenericEventFunctionType,
    /** Standard onFocus event, applied to Flag by AutoDismissFlag */
    onFocus?: GenericEventFunctionType,
    /** Standard onMouseOut event, applied to Flag by AutoDismissFlag */
    onMouseOut?: MouseEventFunctionType,
    /** Standard onMouseOver event, applied to Flag by AutoDismissFlag */
    onMouseOver?: MouseEventFunctionType,
  };

  type FlagGroupProps = {
    /** Flag elements to be displayed. */
    children?: ChildrenType,
    /** Handler which will be called when a Flag's dismiss button is clicked.
     * Receives the id of the dismissed Flag as a parameter.
     */
    onDismissed?: (id: number) => void,
  };

  export class AutoDismissFlag extends React.Component<AutoDismissFlagProps> { }
  export class FlagGroup extends React.Component<FlagGroupProps> { }
  export default class Flag extends React.Component<FlagProps> { }
}

declare module '@atlaskit/layer-manager' {
  export default class LayerManager extends React.Component<{ children: React.ReactNode }> { }
}

declare module '@atlaskit/inline-message' {
  type Position =
    'top left' |
    'top center' |
    'top right' |
    'right top' |
    'right middle' |
    'right bottom' |
    'bottom left' |
    'bottom center' |
    'bottom right' |
    'left top' |
    'left middle' |
    'left bottom';

  type Type =
    'connectivity' |
    'confirmation' |
    'info' |
    'warning' |
    'error';

  type InlineMessageProps = {
    position?: Position;
    secondaryText?: string;
    title?: string;
    type: Type;
  };

  export default class InlineMessage extends React.Component<InlineMessageProps> { }
}

declare module '@atlaskit/single-select' {
  interface ItemType {
    content?: React.ReactNode;
    description?: string;
    label?: string;
    tooltipDescription?: string;
    tooltipPosition?: 'top' | 'bottom' | 'left';
    value?: string | number;
    filterValues?: Array<string>;
    isDisabled?: boolean;
    isSelected?: boolean;
    elemBefore?: React.ReactNode;
  }

  type GroupType = {
    heading?: string;
    items: Array<ItemType>;
  };

  type OpenChangeEvent = {
    event: React.SyntheticEvent<any>;
    isOpen: boolean;
  };

  interface SelectProps {
    appearance?: 'default' | 'subtle';
    droplistShouldFitContainer?: boolean;
    filterValue?: string;
    hasAutocomplete?: boolean;
    id?: string;
    invalidMessage?: React.ReactNode;
    isDisabled?: boolean;
    isFirstChild?: boolean;
    isOpen?: boolean;
    isRequired?: boolean;
    isInvalid?: boolean;
    isLoading?: boolean;
    items?: Array<GroupType>;
    label?: string;
    loadingMessage?: string;
    name?: string;
    noMatchesFound?: string;
    onFilterChange?: (value: string) => void;
    onOpenChange?: (data: OpenChangeEvent) => void;
    placeholder?: string;
    position?: string;
    shouldFocus?: boolean;
    selectedItem?: ItemType;
    shouldFitContainer?: boolean;
    shouldFlip?: boolean;
    maxHeight?: number;
  }

  interface StatelessSelectProps extends SelectProps {
    onSelected?: (item: ItemType) => void;
  }

  interface SingleSelectProps extends SelectProps {
    onSelected?: ({ item }: { item: ItemType }) => void;
    defaultSelected?: ItemType;
  }

  export class StatelessSelect extends React.Component<StatelessSelectProps> { }
  export default class SingleSelect extends React.Component<SingleSelectProps> { }
}

declare module '@atlaskit/checkbox' {
  type Props = {
    /** Sets whether the checkbox begins checked. */
    initiallyChecked?: boolean,
    /** Sets whether the checkbox is disabled. */
    isDisabled?: boolean,
    /** Sets whether the checkbox should take up the full width of the parent. */
    isFullWidth?: boolean,
    /** The label to be displayed to the right of the checkbox. The label is part
     of the clickable element to select the checkbox. */
    label: string,
    /** Marks the field as invalid. Changes style of unchecked component. */
    isInvalid?: boolean,
    /** Function that is called whenever the state of the checkbox changes. It will
    be called with an object containing the react synthetic event as well as the
    new state of the checkbox. */
    onChange?: (data: {
      event?: React.SyntheticEvent<HTMLInputElement>,
      isChecked: boolean,
      name: string,
      value: number | string,
    }) => void,
    /** The name of the submitted field in a checkbox. */
    name: string,
    /** The value to be used in the checkbox input. This is the value that will be returned on form submission. */
    value: number | string,
  };

  export default class Checkbox extends React.Component<Props> { }
}

declare module '@atlaskit/section-message' {
  type Appearance =
    | 'info'
    | 'warning'
    | 'error'
    | 'confirmation'
    | 'change';

  type ActionType = { text: string, onClick?: () => void, href?: string };

  type Props = {
    /* The appearance styling to use for the section message. */
    appearance: Appearance,
    /*
      The main content of the section message. This accepts a react node, although
      we recommend that this should be a paragraph.
    */
    children: React.ReactNode,
    /*
      The heading of the section message.
    */
    title?: string,
    /*
      Actions to be taken from the section message. These accept an object which
      are applied to @atlaskit/button components. Middots are automatically added
      between the items. We generally recommend using no more than two actions.
    */
    actions?: Array<ActionType>,
    /*
      An Icon component to be rendered instead of the default icon for the component.
      This should only be an `@atlaskit/icon` icon. You can check out [this example](/packages/core/section-message/example/custom-icon)
      to see how to provide this icon.
    */
    icon?: JSX.Element,
    /*
      A custom link component. This prop is designed to allow a custom link
      component to be passed to the link button being rendered by actions. The
      intended use-case is for when a custom router component such as react router
      is being used within the application.

      This component will only be used if a href is passed to the action.

      All actions provided will automatically have the linkcomponent passed to them.
    */
    linkComponent?: JSX.Element,
  };

  export default class SectionMessage extends React.Component<Props> { }
}
