import { APIGatewayProxyEvent } from "aws-lambda";
import { LifecyclePayload } from "./types";
import { DynamoDB } from "aws-sdk";
import * as jwt from 'atlassian-jwt';

function getJwtTokenFromQueryParam(event: APIGatewayProxyEvent): string | undefined {
  const parameters = event.queryStringParameters || {};
  return parameters['jwt'];
}

function getJwtTokenFromAuthHeader(event: APIGatewayProxyEvent): string | undefined {
  if (typeof event.headers !== 'object') {
    return undefined;
  }

  const authHeader = event.headers['Authorization'] || event.headers['authorization'];
  if (authHeader === undefined) {
    return undefined;
  }

  if (!authHeader.startsWith('JWT ')) {
    return undefined;
  }

  return authHeader.substring('JWT '.length);
}

export function extractJwtToken(event: APIGatewayProxyEvent): string | undefined {
  const authResult = getJwtTokenFromAuthHeader(event);
  if (authResult !== undefined) {
    return authResult;
  }

  return getJwtTokenFromQueryParam(event);
}

export function isTenantLoadError(rsp: LifecyclePayload | TenantLoadError): rsp is TenantLoadError {
  return 'error' in rsp;
}

export type TenantLoadError = {
  error: string;
};

export async function loadTenant(event: APIGatewayProxyEvent, dynamoDb: DynamoDB.DocumentClient): Promise<LifecyclePayload | TenantLoadError> {
  const installTable = process.env.INSTALL_TABLE;
  if (installTable === undefined) {
    return {
      error: 'The install table was not specified correctly.'
    };
  }

  const token = extractJwtToken(event);
  if (token === undefined) {
    return {
      error: 'Could not find a JWT token on the request.'
    };
  }

  const unverifiedToken = jwt.decode(token, '', true);

  const clientKey = unverifiedToken.iss;
  if (typeof clientKey !== 'string') {
    return {
      error: `The issuer was ${clientKey} but we expected a string.`
    };
  }

  const getTenantParams: DynamoDB.DocumentClient.GetItemInput = {
    TableName: installTable,
    Key: {
      clientKey
    }
  };

  const lifecycleLookup = await dynamoDb.get(getTenantParams).promise();
  const lifecycleLookupResponse = lifecycleLookup.$response;

  if (lifecycleLookupResponse.error) {
    return {
      error: 'Failed to connect to our persistence layer, failed to handle your request'
    };
  }

  // We can now get the existing lifecycle payload
  const existingLifecycle: LifecyclePayload = lifecycleLookup.Item as LifecyclePayload;

  //   validate the JWT token on the request against the shared secret
  try {
    jwt.decode(token, existingLifecycle.sharedSecret);
  } catch (e) {
    return {
      error: 'The token that was provided is invalid.'
    };
  }

  return existingLifecycle;
}