declare namespace AP {
  function resize(): void;

  interface StandardRequestOptions {
      url: string;
      cache?: false;
      headers?: { [header: string]: string };
      // headers TODO
  }

  interface GetRequestOptions extends StandardRequestOptions {
      type: 'GET'
  }

  interface PostPutRequestOptions extends StandardRequestOptions {
      type: 'POST' | 'PUT';
      data: any;
      contentType: string;
  }

  interface DeleteRequestOptions extends StandardRequestOptions {
      type: 'DELETE';
      data?: any;
      contentType?: string;
  }

  interface RequestResponse {
      body: string;
      xhr: XMLHttpRequest;
  }

  function request(options: GetRequestOptions | PostPutRequestOptions | DeleteRequestOptions): Promise<RequestResponse>;

  namespace flag {
      interface Flag {

      }

      interface FlagCreateOptions {
          title: string;
          body: string;
          type?: "info" | "success" | "warning" | "error";
          close?: 'manual' | 'auto';
      }

      function create(options: FlagCreateOptions): Flag;
  }

  namespace dialog {
      interface DialogCommonOptions {
          key: string;
          chrome?: boolean;
          header?: string;
          submitText?: string;
          cancelText?: string;
          hint?: string;
          customData?: object;
          closeOnEscape?: boolean;
      }

      interface DialogSizeOptions extends DialogCommonOptions {
          size: 'small' | 'medium' | 'large' | 'x-large' | 'fullscreen';
      }

      interface DialogDimensionOptions extends DialogCommonOptions {
          width: string;
          height: string;
      }

      interface Dialog {
          on: (event: 'close', callbackFn: () => void) => void;
      }

      function create(options: DialogSizeOptions | DialogDimensionOptions) : Dialog;

      function close(data?: object): void;
  }

  namespace events {
      type EventData = object;

      type EventListner = (data: EventData) => void;
      type EventFilter = (data: EventData) => boolean;

      function on(name: string, listener: EventListener): void;

      function onPublic(name: string, listener: EventListener, filter: EventFilter): void;

      function once(name: string, listener: EventListener): void;

      function oncePublic(name: string, listener: EventListener, filter: EventFilter): void;

      function onAny(listener: EventListener): void;

      function onAnyPublic(listener: EventListener, filter: EventFilter): void;

      function off(name: string, listener: EventListener): void;

      function offPublic(name: string, listener: EventListener): void;

      function offAll(name: string): void;

      function offAllPublic(name: string): void;

      function offAny(listener: EventListener): void;

      function offAnyPublic(listener: EventListener): void;

      function emit(name: string, ...args: EventData[]): void;

      function emitPublic(name: string, ...args: EventData[]): void;
  }

  namespace jira {
    function getWorkflowConfiguration(callback: (config: string) => void): void;

    namespace WorkflowConfiguration {
      function onSaveValidation(listener: () => boolean): void;
      /**
       * Save the workflow configuration for the current screen.
       *
       * @param listener It is expected that the listener will return a string that will work with JSON.parse.
       */
      function onSave(listener: () => string): void;

      interface TriggerResult {
        valid: boolean;
        value: string | undefined
      }

      function trigger(): TriggerResult;
    }
  }
}