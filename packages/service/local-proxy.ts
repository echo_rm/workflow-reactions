import * as http from 'http';
import * as httpProxy from 'http-proxy';
import { Omit } from 'type-zoo';
//
// Create your proxy server and set the target in the options.
//
const proxy = httpProxy.createProxyServer({target:'http://localhost:9220'}).listen(9229); // See (†)

type ServerProxyWithPath = {
  name: string;
  mountPath: string;
  proxyFrom: string;
};

type ServerProxy = Omit<ServerProxyWithPath, 'mountPath'>;

type ServerProxies = {
  proxies: ServerProxyWithPath[];
  defaultProxy: ServerProxy;
};

const configuration: ServerProxies = {
  proxies: [{
    name: 'backend',
    mountPath: '/api',
    proxyFrom: 'http://localhost:9222'
  }],
  defaultProxy: {
    name: 'frontend',
    proxyFrom: 'http://localhost:9221'
  }
};

http.createServer(function (req, res) {
  const getProxy = configuration.proxies.find(p => req.url !== undefined && req.url.startsWith(p.mountPath));

  const serverProxy: ServerProxy = getProxy !== undefined ? getProxy : configuration.defaultProxy;

  proxy.web(req, res, { target: serverProxy.proxyFrom }, (err, req, res) => {
    res.statusCode = 500;
      res.write(JSON.stringify({
        proxy: {
          error: `Failed to proxy to: '${serverProxy.name}'`
        },
        backing: {
          error: err.message
        }
      }));
      res.end();
  });
}).listen(9220);