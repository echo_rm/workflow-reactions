/**
 * This code is pretty much a direct clone of: https://github.com/99xt/serverless-dynamodb-client/blob/master/index.js
 */
import * as AWS from 'aws-sdk';

const localOptions = {
  region: "localhost",
  endpoint: "http://localhost:9223" // This is the port we configured dynamo to use.
};

var isOffline = function (): boolean {
    // Depends on serverless-offline plugion which adds IS_OFFLINE to process.env when running offline
    return !!process.env.IS_OFFLINE;
};

export function createDocumentCilent(): AWS.DynamoDB.DocumentClient {
  return isOffline() ? new AWS.DynamoDB.DocumentClient(localOptions) : new AWS.DynamoDB.DocumentClient();
}

export function createDynamoDB(): AWS.DynamoDB {
  return isOffline() ? new AWS.DynamoDB(localOptions) : new AWS.DynamoDB();
}